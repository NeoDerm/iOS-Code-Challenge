# iOS-Code-Challenge

Create a new XCode project for complete the code challenge

## Environment:

- Latest XCdode - Version 9.4.1 or above
- Language - Swift 4


## Programming Challenge:

Build a simple top free game listing as iOS App Store with following requirements:

- Use Data Model for local storage
- Lazy loading for each 10 items
- Scrolling list smoothly

You can use any library or framework for the project.


### Data Resources
https://rss.itunes.apple.com/api/v1/hk/ios-apps/top-free/games/100/explicit.json

### Layout Reference
![reference](/reference.png)

## Submission

Please send back the full source code XCode project via email or any public storage (e.g. gitlab, github).